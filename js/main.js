
function showNavigation(){
	$('.header .navigation').slideDown();
	$('.header .screen-mask').show();
}


function hideNavigation() {
	$('.header .navigation').slideUp();
	$('.header .screen-mask').hide();
}

$(document).ready(function() {



	$(window).scroll(function(e) {
		// console.log('abc: '+ $('body').scrollTop() )
		if($('body').scrollTop() >150){
			$('.header .menu-toggle').addClass('fixed')
		}else{
			$('.header .menu-toggle').removeClass('fixed')
		}
	})


	$('.header .menu-toggle').click(function() {
		showNavigation()
	})

	$('.header .screen-mask').click(function() {
		hideNavigation()
	})

	$('.header .hide-menu').click(function() {
		hideNavigation()
	})




})
